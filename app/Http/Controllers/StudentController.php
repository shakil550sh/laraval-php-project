<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Image;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students=Student::all();
        return view('Student.view',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$Student=Student::all();  ,compact('Student'))
        return view('Student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $request->validate([
        'std_name' => 'required|max:255|min:3',
        'std_id' => 'required',
    ]);


        $std=new Student;

        if($request->hasfile('image'))
        {

            $image=$request->file('image');
            $file_name=time().'.'.$image->getClientOriginalExtension();
           

            $image_resize= Image::make($image->getRealPath());
            $image_resize->resize(300,400);

            

            $image_resize->save('images/'.$file_name);

           
             $std->image=$file_name;
            
        }
        $std->std_name=$request->std_name;
        $std->std_id=$request->std_id;
        $std->save();

        
        return back()->with('success','Data Insert Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= Student::find($id);
        return view('Student.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $std=Student::find($id);

        if($request->hasfile('image'))
        {
            $image=$request->file('image');
            $file_name=time().'.'.$image->getClientOriginalExtension();

             $old_file= $std ->image;

            $image_resize= Image::make($image->getRealPath());
            $image_resize->resize(300,400);

                if(!empty($old_file)){
                     $path=("images/$old_file");
                unlink($path);
                }

            $image_resize->save('images/'.$file_name);

             $std->image=$file_name;            
            
        }
        $std->std_name=$request->std_name;
        $std->std_id=$request->std_id;
         $std->save();

        return back()->with('success','Done Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Student::find($id);
        $old_file=$data->image;

           if(!empty($old_file)){
             $path=("images/$old_file");
             unlink($path);
          }
        $data->delete();

        return back()->with('success','Data Deleted successfully');

    

    }
}
