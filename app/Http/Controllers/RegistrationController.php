<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Sentinel;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('authentication.registration');
    }
    public function register(Request $request)
    {
          
    $request->validate([
    'first_name' => 'required|max:70',
    'last_name' => 'required|max:70',
    'email' =>  'required|unique:users',
    'mobile' => 'required',
    'password' => 'required|min:4',
    'confirm_password' => 'required_with:password|same:password|min:4',
]
// ,
// // write here the error massage you want to see
// [
//    'email.unique'=> 'your Email Is used. Please try a new Email !!!',
//    'confirm_password.same' =>'Confirm Passowerd are not Match with passowerd'
// ]
);
      $user=Sentinel::registerAndActivate($request->all());
       
      $role =Sentinel::findRoleBySlug('member');

      $role->users()->attach($user);
     
      return redirect()->back()->with('success','Registration successful');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
