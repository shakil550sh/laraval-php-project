<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;

class DepartmentController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}

    public function index()
    {

        $departments=Departments::orderBy('id','desc')->paginate();
        return view('department.view',compact('departments'));
    }
     public function create()
    
    {
        $departments=Departments::orderBy('id','desc')->paginate();
        return view('department.create',compact('departments'));
    }
    public function store(Request $request)
    {
        $request->validate([
        'dpt_name' => 'required|unique:departments|max:255|min:3',
        'dpt_code' => 'required',
    ]);

        $data=$request->all();
        Departments::create($data);
        return back()->with('success','Data Insert Successfylly');
    }

    public function destroy($id)
    {
        $data=Departments::find($id);
        $data->delete();
        return back()->with('success','Data delete success');
    }

    public function edit($id)
    {
        $data=Departments::find($id);
        return view('department.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {
        $data=Departments::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return redirect('/department/create')->with('success','data update Successfylly');

    }
}
