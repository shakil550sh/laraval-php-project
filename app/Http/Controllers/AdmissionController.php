<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;
use App\StdAdmission;


class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admission = StdAdmission::join('departments','std_admissions.departments_id','=','departments.id')->select('std_admissions.*','departments.dpt_name')->get();
        return view('Admission.view',compact('admission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department=Departments::all();
        return view('Admission.create',compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        StdAdmission::create($data);
        return back()->with('success','Done');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=StdAdmission::where('id',$id)->first();
         $department=Departments::all();
        return view('Admission.edit',compact('data','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=StdAdmission::find($id);
        $new_data= $request->all();
        $data->update($new_data);
        return redirect('/StdAdmission')->with('success','Update done');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
