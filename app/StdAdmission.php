<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class StdAdmission extends Model
{
     protected $fillable=['departments_id','std_name','mobile','address'];
     
     public function departments()
     {
     	return $this->belongsTo(Departments::class);
     }
}
