<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','Frontendcontroller@view');
Route::get('/signUp','RegistrationController@create');
Route::post('/signUp','RegistrationController@register');
Route::get('/loginUser','LogInController@loginForm');
Route::post('/loginUser','LogInController@login');
Route::get('/logout','LogInController@logout');

//department start
// Route::get('/department/create','DepartmentController@create');

// Route::post('/department','DepartmentController@store');

// Route::get('/department','DepartmentController@index');

// Route::get('/department/delete/{id}','DepartmentController@destroy');
// Route::delete('/department/{id}','DepartmentController@destroy');

// Route::get('/department/{id}/edit','DepartmentController@edit');

// Route::patch('/department/{id}','DepartmentController@update');

Route::resource('department','DepartmentController');
//department end


Route::get('/StdAdmission/create','AdmissionController@create');
Route::post('/StdAdmission','AdmissionController@store');
Route::get('/StdAdmission','AdmissionController@index');
Route::get('/StdAdmission/{id}/edit','AdmissionController@edit');

Route::patch('/std_admissions/{id}','AdmissionController@update');

Route::delete('/StdAdmission/{id}','AdmissionController@destroy');

// Student Controller path

// Route::get('/Student/create','StudentController@create');
// Route::post('/Student','StudentController@store');
// Route::get('/Student','StudentController@index');
Route::resource('/Student','StudentController');


Route::get('/Bangladesh/create','BangladeshController@create');
Route::post('/Bangladesh','BangladeshController@store');
Route::get('/Bangladesh','BangladeshController@index');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('categories','Admin\CategoryController');
Route::resource('posts','Admin\PostController');


//demo form design with CRUD
Route::resource('demoform','DemoFormController');

Route::get('/blogs','Frontend\BlogController@view');

Route::get('/blogDetails/{id}','Frontend\BlogController@details');

Route::post('/comment','Frontend\CommentController@store'); 
