@extends('Layout.FrontEnd.Master')
@section('title','Student view')
@section('content')
<br>
	
	<h2><a href="/Student/create">Add Student</a></h2>@include('massage.massage') 
	<table border="1">

		<tr>
			<td>Name</td>
			<td>Photo</td>
			<td>Edit</td>
			<td>Delete</td>
		</tr>
		@foreach($students as $data)
		<tr>
			<td>{{$data->std_name}}</td>
			<td> <img src="{{asset('images/'.$data->image)}}" height="100"></td>

			<td> <a href="/Student/{{$data->id}}/edit">Edit</a> </td>
			<td>
				{!! Form::open(['url' => '/Student/'.$data->id,'method'=>'Delete']) !!}
	   			 <button type="submit" class="btn btn-warning" onclick="return confirm('are you sure?')">Delete</button>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
		
	</table>

@endsection