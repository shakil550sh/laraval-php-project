@extends('Layout.FrontEnd.Master')

@section('title','Student Update')
@section('content')

<br>
<br>
<h2>Update</h2>
@include('massage.massage') 

	{!! Form::open(['url' => '/Student/'.$data->id,'method'=>'PATCH','class'=>'form-group','enctype'=>'multipart/form-data']) !!}]) !!}
		
		<input type="text" name="std_name" placeholder="Student name" value="{{$data->std_name}}">
		<input type="text" name="std_id" placeholder="Student id" value="{{$data->std_id}}">
		<!-- <input type="text" name="mobile" placeholder="Mobile">
		<input type="email" name="email" placeholder="Email"> -->
		<input type="file" name="image">
		<img src="{{asset('images/'.$data->image)}}" height="100"/>
		<input type="submit" value="save & changes">

	{{Form::close()}}
@endsection