@extends('design.frontend.master')
@section('title','Add POST')

@section('content')

<div class="container">
  @include('massage.massage')
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
  <h2>POST Now</h2>
  <form action="/posts" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="form-group">
      <label for="fname">Select a Category</label>
      <select name="categories_id" class="form-control">
      	<option value="">Select a Category</option>
      	@foreach($category as $cat)
      	<option value="{{$cat->id}}">{{$cat->category}}</option>
      	@endforeach
      </select>
    </div>

     <div class="form-group">
      <label for="fname">Upload a Photo</label>
      <input type="file" class="form-control" id="fname" placeholder="image upload" name="image">
    </div>

    <div class="form-group">
      <label for="fname">Title</label>
      <input type="text" class="form-control" id="fname" placeholder="post title" name="title">
    </div>
    
    <div class="form-group">
      <label for="fname">Description</label>
      <textarea class="form-control" name="description"></textarea>     
    </div>
    
 
    <button type="submit" class="btn btn-default">Post</button>
  </form>

</div></div>
<!-- End Row -->
<br>

<!--start Row-->

<div class="row">
	
	<div class="col-md-12">

		<table class="table table-bordered table-hover">
			
			<thead>
				<tr>
					<th>SL</th>
					<th>Category name</th>
					<th>Title</th>
					<th>Description</th>
					<th>Image</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>				
			</thead>
			<tbody>
				@foreach($posts as $key=>$data)
				<tr>
					<td>{{++$key}}</td>
					<td>{{$data->categories->category}}</td>
					<td>{{$data->title}}</td>
					<td>{{substr($data->description,0,60)}}.....</td>
					<td>
						<img src="{{asset('images/'.$data->image)}}" width="150">
					</td>
					
					<td>
						<a href="/posts/{{$data->id}}/edit">Edit</a>
					</td>
					<td>
						{!! Form::open(['url' => '/posts/'.$data->id,'method'=>'Delete']) !!}
	   					 <button type="submit" onclick="return confirm('are you sure?')">Delete</button>

						{!! Form::close() !!}
			</td>
				</tr>	
				@endforeach			
				</tbody>
		</table>		
	</div>
</div>
<!-- End Row-->
</div>
<br>
@endsection