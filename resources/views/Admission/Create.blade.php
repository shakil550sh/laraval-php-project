@extends('design.frontend.master')

@section('title','Admissions Create')
@section('content')

	<h1>Admission Registration</h1>

@include('massage.massage')

	<form action="/StdAdmission" method="POST">
			{{csrf_field()}}
		<select name="departments_id">
			<option value="">Select A Department </option>
			@foreach($department as $data)
			<option value="{{$data->id}}">{{$data->dpt_name}}</option>
		@endforeach
		</select>
			

		<input type="text" name="std_name" placeholder="Student Name">
		<input type="text" name="mobile" placeholder="Mobile Number">
		<textarea name="address" placeholder="address"></textarea>

		<input type="Submit" value="SAVE">
	</form>
	
@endsection 