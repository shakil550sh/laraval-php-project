@extends('design.frontend.master')

@section('title','Admissions Update')
@section('content')

	<h1>Edit Admission Students Profile</h1>

@include('massage.massage')
	
			{!! Form::open(['url' => '/std_admissions/'.$data->id, 'method'=>'PATCH','class'=>'form-group','enctype'=>'multipart/form-data']) !!}

                                  
                                
		<select name="departments_id">

 				 

			<!-- <option value="{{$data->departments_id}}">{{$data->dpt_name}}</option> -->

			@foreach($department as $datad)
				@if($datad->id==$data->departments_id)
					<option value="{{$data->id}}" selected="">{{$data->departments->dpt_name}}</option>
				@else
					<option value="{{$datad->id}}">{{$datad->dpt_name}}</option>
					@endif
			@endforeach
		</select>  
			

		<input type="text" name="std_name" placeholder="Student Name" value="{{$data->std_name}}">
		<input type="text" name="mobile" placeholder="Mobile Number" value="{{$data->mobile}}">
		<textarea name="address" placeholder="address">{{$data->address }}</textarea>

		<input type="Submit" value="Update">

	{{Form::close()}}
	
@endsection 