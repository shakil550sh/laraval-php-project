@extends('design.frontend.master')


@section('title', 'Admissions Student view')

@section('content')
<br><br>
<h1>Admission Student Profile</h1>
@include('massage.massage')
	<table border="1">
		<tr>
			<th>Si</th>
			<th>Department</th>
			<th>Std Name</th>
			<th>mobile</th>
			<th>Address</th>
			<th>Edit</th>
			<th>Delete</th>
			
		</tr>
		
		@foreach($admission as $key=> $data)
		
		<tr>
			<td> {{++$key}} </td>
			<td>{{$data->dpt_name}}</td>
			<td>{{$data->std_name}}</td>
			<td>{{$data->mobile}}</td>
			<td>{{$data->address}}</td>

<!--if not correct write this StdAdmission / std_admissions-->
			<td><a href="/StdAdmission/{{$data->id}}/edit">Edit</a></td>
			<td>
				{!! Form::open(['url' => '/StdAdmission/'.$data->id,'method'=>'Delete']) !!}
	   			 <button type="submit" onclick="return confirm('are you sure?')">Delete</button>
				{!! Form::close() !!}
			</td>
			
		</tr>

		@endforeach
	</table>


@endsection