@extends('design.frontend.master')

@section('title', 'Departments Student view')

@section('content')
<br>
<br>

	<h1>Department Create</h1> <h3> <a href="/department">View department</a> </h3>
	<h3> <a href="/">HOME</a> </h3>


 @if ($errors->any())
     @foreach ($errors->all() as $error)
         <div>{{$error}}</div>
     @endforeach
 @endif

 @if(session()->has('success'))
    <div class="alert alert-success" style="color: green;font-weight: bold;">
        {{ session()->get('success') }}
    </div>
@endif

	

	<form action="/department" method="POST">
		{{csrf_field()}}
		
		<input type="text" name="dpt_name" placeholder=" department name">
		<input type="text" name="dpt_code" placeholder="department code">
		<input type="submit" value="save">

	</form>
	@include('massage.massage')
	
	<br>
	<br>
	<br>
	<br>


	<table border="1">
		
		<tr>
			<th>SI</th>
			<th>Dpt Name</th>
			<th>Dpt Code</th>
			<th>Action</th>			
			<th>Delete</th>			
		</tr>
		<?php $i= $departments->perpage()*($departments->currentPage()-1); ?>
		@foreach($departments as $key=>$data)
		<tr>
			<td>{{++$i}}</td>
			<td>{{$data->dpt_name}}</td>
			<td>{{$data->dpt_code}}</td>
			<td><a href="/department/{{$data->id}}/edit">Edit</a> | </td>
			<td>
				{!! Form::open(['url' => '/department/'.$data->id,'method'=>'Delete']) !!}
	   			 <button type="submit" onclick="return confirm('are you sure?')">Delete</button>
				{!! Form::close() !!}
			</td>

		</tr>

		@endforeach

	</table>

	{{$departments->links()}}

@endsection