@extends('design.frontend.master')


@section('title', 'Departments Student Edit')

@section('content')
<br>
<br>

	<h1>Department Edit</h1> <h3> <a href="/department">View department</a> </h3>


	<form action="/department/{{$data->id}}" method="POST">
		{!! Form::open(['url' => '/department/'.$data->id,'method'=>'PATCH']) !!}
		
		
		<input type="text" name="dpt_name" placeholder="department name" value="{{$data->dpt_name}}">
		<input type="text" name="dpt_code" placeholder="department code" 
		value="{{$data->dpt_code}}">
		<input type="submit" value="Update">

	{!! Form::close() !!}
	<br>
	<br>
	<br>
	<br>
	@include('massage.massage')

	
@endsection