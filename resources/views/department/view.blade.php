@extends('design.frontend.master')
@section('title', 'Departments Student view')

@section('content')
<br>
<br>

	<h1>Department View</h1> <h3> <a href="/department/create">Add new department</a></h3>
	<h3> <a href="/">HOME</a> </h3>

	 @if ($errors->any())
     @foreach ($errors->all() as $error)
         <div>{{$error}}</div>
     @endforeach
 @endif

@if(session()->has('success'))
    <div class="alert alert-success" style="color: green; font-weight: bold;">
        {{ session()->get('success') }}
    </div>
@endif


	<!-- <form action="/department" method="POST">
		{{csrf_field()}}
		
		<input type="text" name="dpt_name" placeholder="department name">
		<input type="text" name="dpt_code" placeholder="department code">
		<input type="submit" value="save">

	</form> -->
	<br>
	<br>
	<br>
	<br>

	<table border="1">
		
		<tr>
			<th>SI</th>
			<th>Dpt Name</th>
			<th>Dpt Code</th>
			<th>Action</th>			
		</tr>

		@foreach($departments as $key=>$data) 
		<!-- problem in department where it come from?? -->
		<tr>
			<td>{{++$key}}</td>
			<td>{{$data->dpt_name}}</td>
			<td>{{$data->dpt_code}}</td>
			<td>Edit | Delete</td>
		</tr>

		@endforeach

	</table>

	{{$departments->links()}}
	
@endsection